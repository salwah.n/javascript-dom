//document.createElement
//buat element baru
const pBaru = document.createElement('p');
const teksPBaru = document.createTextNode('paragraf baru');
//simpam tulisan ke dalam paragraf
//node.appendChild
pBaru.appendChild(teksPBaru);
//simpan pBaru diakhir section A
const sectionA = document.getElementById('a');
sectionA.appendChild(pBaru);



//document.createTextNode
const listBaru = document.createElement('li');
const teksLiBaru =  document.createTextNode('item baru');
listBaru.appendChild(teksLiBaru);

const ul = document.querySelector ('section#b ul');
const li2 = ul.querySelector('li:nth-child(2)');
//element.insertBefore
ul.insertBefore(listBaru, li2);



//parentNode.remove
const link = document.getElementsByTagName('a') [0];
sectionA.removeChild(link);

//parentNode.replaceChild
const sectionB = document.getElementById('b');
const p4 = sectionB.querySelector('p');

const h2Baru = document.createElement;
const teksH2Baru = document.createTextNode('judul baru !!');

h2Baru.appendChild(teksH2Baru);
sectionB.replaceChild(h2Baru, p4);


