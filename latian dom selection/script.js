//DOM selection

//document.getElementById() -> mengembalikan element

//const judul = document.getElementById('judul');
//judul.style.color = 'yellow';
//judul.style.backgroundColor = 'brown';
//judul.innerHTML = 'salwa h.n';



//document.getElementsByTagName() -> mengembalikan HTMLCollections

//const p = document.getElementsByTagName('p');
//for (let i = 0; i < p.length ;i++) {
//    p[i].style.backgroundColor = 'lightblue';
//}

//const h1 = document.getElementsByTagName('h1')[0];
//h1.style.fontSize = '50px';



//document.getElementsByClassName()  -> mengembalikan HTMLCollections

//const p1 = document.getElementsByClassName('p1');
//p1[0].innerHTML = 'ini diubah dr javascript';



//document.querySelector()-> element
const p4 = document.querySelector('p')
p4.style.color = 'green';
p4.style.fontSize = '30px';

const li2 = document.querySelector('selection#b ul li:nth-child(2)');




//document.querySelectorAll() ->ini tipenyaa nodelist
const p = document.querySelectorAll('p');
for (let i = 0; i < p.length ;i++){
    p[i].style.backgroundColor = 'lightblue';
}